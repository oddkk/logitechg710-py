import os

def setMacro(val):
	fs = os.open("/sys/bus/hid/devices/0003:046D:C24D.0003/logitech-g710/led_macro",os.O_NONBLOCK|os.O_WRONLY)
	os.write(fs,str(val))
	os.close(fs)

def setKeys(key,wasd):
	fs = os.open("/sys/bus/hid/devices/0003:046D:C24D.0003/logitech-g710/led_keys",os.O_NONBLOCK|os.O_WRONLY)
	os.write(fs,str(wasd << 4 | key))
	os.close(fs)
